namespace LemRover{
	class Arm
	{
	public:
		Arm();
		virtual ~Arm();

		void setActive(bool isActive);
		bool getActive();

		void setLength(double length);
		double getLength();

		void setFirstAngle(double angleSize);
		double getFirstAngle();

		void setSecondAngle(double angleSize);
		double getSecondAngle();

		void setThirdAngle(double angleSize);
		double getThirdAngle();


		void setPowerOne(double power);
		double getPowerOne();

		void setPowerTwo(double power);
		double getPowerTwo();

		void setPowerThree(double power);
		double getPowerThree();

	protected:
	private:
		double _length;
		bool _isActive;
		double _firstAngle;
		double _secondAngle;
		double _thirdAngle;
		double _powerOne;
		double _powerTwo;
		double _powerThree;
	};
}
