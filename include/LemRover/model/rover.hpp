#include "arm.hpp"
#include "wheel.hpp"

namespace LemRover{
	class Rover {
	public:
		Rover();

		void setWheelsDiameter(double newDiameter);
		double getWheelsDiameter();

		Wheel::wheelDescription getWheel(Wheel::wheelDescription whichWheel);

		void setWheelPower(Wheel::wheelDescription whichWheel, double newPower);
		double getWheelPower(Wheel::wheelDescription whichWheel);

		void setWheelSpeed(Wheel::wheelDescription whichWheel, double newSpeed);
		double getWheelSpeed(Wheel::wheelDescription whichWheel);

		void setWheelPosition(Wheel::wheelDescription whichWheel, float x, float y, float z);
		std::vector<float> getWheelPosition(Wheel::wheelDescription whichWheel);

		void setArmActive(bool isActive);
		bool getArmActive();

		void setArmLength(double newLength);
		double getArmLength();

		void setArmFirstAngle(double newAngle);
		double getArmFirstAngle();

		void setArmSecondAngle(double newAngle);
		double getArmSecondAngle();

		void setArmThirdAngle(double newAngle);
		double getArmThirdAngle();

		void setArmFirstPower(double newPower);
		double getArmFirstPower();

		void setArmSecondPower(double newPower);
		double getArmSecondPower();

		void setArmThirdPower(double newPower);
		double getArmThirdPower();
		virtual ~Rover();
	private:
		double _wheelsDiameter;
		Wheel* mWheels;
		Arm* mArm;

	};
}
