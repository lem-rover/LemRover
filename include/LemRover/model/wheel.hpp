#include <vector>

namespace LemRover{
	class Wheel

	{
	public:
		Wheel();
		virtual ~Wheel();
		enum wheelDescription
		{
			leftFrontWheel = 0,
			leftMiddleWheel = 1,
			leftRearWheel = 2,
			rightFrontWheel = 3,
			rightMiddleWheel = 4,
			rightRearWheel = 5
		};
		void setWheelDesription(wheelDescription whichWheel);
		wheelDescription getWheelDesription(void);

		void setSpeed(double speed);
		double getSpeed(void);

		void setWheelPower(double wheelPower);
		double getWheelPower(void);

		void setPosition(float x, float y, float z);
		std::vector<float> getPosition(void);

	protected:
	private:
		double _speed;
		wheelDescription _whichWheel;
		double _wheelPower;
		std::vector <float> _points;

	};
}
