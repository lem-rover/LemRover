#ifndef WIDGETIMAGE_H
#define WIDGETIMAGE_H

#include <QtWidgets/QWidget>
#include <QtWidgets/QLabel>

namespace LemRover{
class WidgetImage : public QWidget
{
    Q_OBJECT
public:
    explicit WidgetImage(QWidget *parent = 0);

    bool loadImage(QString& pPath);
Q_SIGNALS:

private :
    QPixmap *mPixelMap;
    QLabel * mLabel;

public Q_SLOTS:
};
}
#endif // WIDGETIMAGE_H
