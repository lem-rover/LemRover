#ifndef GRAPHWIDGET_H
#define GRAPHWIDGET_H

#include <QtWidgets>

namespace LemRover{

class GraphicsElement;
class GuiElement;
class Window;

#define mPositionFix 100
#define sBoxWidth 180
#define sBoxHeight 80
#define sMargin 3
#define sSpacing 3


class RoverWidget : public QGraphicsView
{
    Q_OBJECT

public:

    RoverWidget(Window* window, QWidget *parent = 0);
protected:
    void resizeEvent(QResizeEvent *event) {
        if (scene())
            scene()->setSceneRect(QRect(QPoint(0, 0), event->size()));
        QGraphicsView::resizeEvent(event);
    }

    int timerId;
    Window * mWindow;
};

}
#endif // GRAPHWIDGET_H
