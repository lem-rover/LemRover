#include <QGraphicsItem>

class QGraphicsSceneMouseEvent;
namespace LemRover{
    class RoverWidget;

    class GuiElement: public QGraphicsItem
    {
    public:
        GuiElement( RoverWidget* pWidget , int pWidth = 180, int pHeight = 80);
        QRectF boundingRect() const ;//Q_DECL_OVERRIDE;
        QPainterPath shape() const ;//Q_DECL_OVERRIDE;
        QPixmap *getPixelMap();
        void paint( QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *widget ) ;//Q_DECL_OVERRIDE;
        void setSize(int pWidth, int pHeight);
        void setPosition(int pX, int pY);
        void setInfo(QString *set);
    protected:
        void mouseMoveEvent( QGraphicsSceneMouseEvent *event ) ;//Q_DECL_OVERRIDE;
        void mousePressEvent( QGraphicsSceneMouseEvent *event ) ;//Q_DECL_OVERRIDE;
        void mouseReleaseEvent( QGraphicsSceneMouseEvent *event ) ;//Q_DECL_OVERRIDE;

    private:
        QPixmap *mPixelmap ;
        bool isPressed;
        QPointF *mPos;
        int mHeight;
        int mWidth;
        RoverWidget *mWidget;
        QString mCurrentInformation;
    };
}
