#include <QtWidgets/QMainWindow>
#include <QtWidgets/QListWidget>
#include <QtWidgets/QMdiArea>
#include <QtWidgets/QProgressBar>

#include "gui/internalwindow.hpp"
#include "gui/widgets/roverwidget.hpp"
#include "logic/controller.hpp"
#include "qnode.hpp"


namespace LemRover{

class Window : public QMainWindow {
Q_OBJECT
private:
    QNode qnode;
    RoverWidget *lemWidget ;
    QMdiArea *panelCentral;
    QListWidget *informationList;
    QProgressBar *barX;

    //MenuField - każde pole jest oddzielną opcją menu kontekstowego
    QMenu *fileMenu;
    QMenu *viewMenu;
    QMenu *helpMenu;
    //MenuFields Koniec

    //ToolBars - każde pole jest zestawem kategorycznie podzielonych narzędzi
    QToolBar *fileToolBar;
    QToolBar *editToolBar;
    //ToolBars Koniec

    /*Actions*/
    QAction *newLetterAct;
    QAction *actionSwitchGamepad;
    QAction *aboutAct;
    QAction *actionQuit;
    /*End of Actions*/

    Controller *mController;
    bool isGamepadEnabled;
    QIcon *mGamepadEnabled;
    QIcon *mGamepadDisabled;
    QFrame *frame;
    QVBoxLayout *verticalLayout;
    QVBoxLayout *verticalLayout_3;
    QGroupBox *groupBox;
    QGridLayout *gridLayout;
    QLabel *label;
    QLabel *label_2;
    QLabel *label_3;
    QCheckBox *checkbox_use_environment;
    QCheckBox *checkbox_remember_settings;
    QPushButton* button_connect;
    QLineEdit *line_edit_master;
    QLineEdit *line_edit_topic;
    QLineEdit *line_edit_host;
    QWidget* dockWidgetContents_2;
    public:
        Window(int argc, char** argv, QWidget *parent = 0);
    private Q_SLOTS:
        void slot_got_Gamepad_info( QStringList info );
        void slot_set_connected();
        void slot_set_disconnected();
        void switchGamepad();
        void slot_controller_connection_closed();
        void slot_controller_value_changed( QString xml );

    Q_SIGNALS:
        void signal_keyboard_val_change( const QString & );

public Q_SLOTS:
        void on_button_connect_clicked(bool check );
        void on_checkbox_use_environment_stateChanged(int state);
    private:
        //Podzielone kategorycznie metody odpowiadające za budowę GUI
        void createROSControl();
        void createActions();
        void createMenus();
        void createToolBars();
        void createStatusBar();
        void connectROS();
        void showNoMasterMessage();
        void WriteSettings();
        void ReadSettings();
        void createDockWindows();
        InternalWindow *activeMdiChild();

        void updateMenus();
protected:
        void closeEvent(QCloseEvent *);
};
}
