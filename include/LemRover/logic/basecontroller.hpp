#ifndef BASECONTROLLER_H
#define BASECONTROLLER_H

#include <QObject>
namespace LemRover{
class BaseController : public QObject
{
    Q_OBJECT
public:
    explicit BaseController(QObject *parent = 0);
    virtual QString xmlcreator(QString type, int number, int value);
Q_SIGNALS:

public Q_SLOTS:
};
}


#endif // BASECONTROLLER_H
