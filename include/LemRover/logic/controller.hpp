#ifndef CONTROLLER_H
#define CONTROLLER_H

#include <QObject>

#include "gamepads/padreader.hpp"
#include "basecontroller.hpp"

namespace LemRover{
class Controller : public QObject
{
    Q_OBJECT
public:
    explicit Controller(QObject *parent = 0);
    Padreader *pad_reader;
    BaseController *actual_controller;
    void set_type(QString typ);
private:
    QStringList Controllerinfo;
Q_SIGNALS:
    void signal_up_controller_value_changed(QString xml);
    void signal_controller_connection_closed();
    void sinal_destroy_pad();
    void signal_up_got_Gamepad_info(QStringList info);
public Q_SLOTS:
    void slot_keyboard_value_changed(QString type);
    void slot_controller_value_changed(QString type, int number, int value);
    void slot_controller_connection_closed();
    void slot_destroy_pad();
    void slot_got_Gamepad_info(QStringList info);
};
}
#endif // CONTROLLER_H
