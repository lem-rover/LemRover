/**
 * @file /include/LemRover/qnode.hpp
 *
 * @brief Communications central!
 *
 * @date February 2011
 **/
/*****************************************************************************
** Ifdefs
*****************************************************************************/

#ifndef LemRover_QNODE_HPP_
#define LemRover_QNODE_HPP_

/*****************************************************************************
** Includes
*****************************************************************************/

#include <ros/ros.h>
#include <string>
#include <QThread>
#include <QStringListModel>


/*****************************************************************************
** Namespaces
*****************************************************************************/

namespace LemRover {

/*****************************************************************************
** Class
*****************************************************************************/

class QNode : public QThread {
    Q_OBJECT
public:
	QNode(int argc, char** argv );
	virtual ~QNode();
	bool init();
	bool init(const std::string &master_url, const std::string &host_url);
	void run();

	/*********************
	** Logging
	**********************/
	enum LogLevel {
	         Debug,
	         Info,
	         Warn,
	         Error,
	         Fatal
	 };

	QStringListModel* loggingModel() { return &logging_model; }
	void log( const LogLevel &level, const std::string &msg);

    void publishVel(float velX, float velY, float velZ);
Q_SIGNALS:
	void loggingUpdated();
    void rosShutdown();

private:
	int init_argc;
    char** init_argv;
    ros::Publisher chatter_publisher;
    //ros::Subscriber chatter_subscriber;
    QStringListModel logging_model;

    float angX, angY, angZ;
    float linX, linY, linZ;
};

}  // namespace LemRover

#endif /* LemRover_QNODE_HPP_ */
