#include "../../include/LemRover/model/wheel.hpp"

#include <vector>
#include <iostream>
using namespace LemRover;


Wheel::Wheel()
{
	std::cout << "Wheel is being added" << std::endl;
}

void Wheel::setPosition(float x, float y, float z)
{
	_points.push_back(x);
	_points.push_back(y);
	_points.push_back(z);
}

std::vector<float> Wheel::getPosition()
{
	return _points;
}

void Wheel::setWheelDesription(wheelDescription whichWheel)
{
	_whichWheel = whichWheel;
}

Wheel::wheelDescription Wheel::getWheelDesription(void)
{
	return _whichWheel;
}


void Wheel::setSpeed(double speed)
{
	_speed = speed;
}

double Wheel::getSpeed(void)
{
	return _speed;
}

void Wheel::setWheelPower(double wheelPower)
{
	_wheelPower = wheelPower;
}

double Wheel::getWheelPower(void)
{
	return _wheelPower;
}

Wheel::~Wheel()
{
	std::cout << "Wheel has been deleted" << std::endl;
}

