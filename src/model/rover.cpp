#include "../../include/LemRover/model/rover.hpp"
#include <iostream>

using namespace LemRover;

Rover::Rover() {
	std::cout << "Rover has been created." << std::endl;
	mWheels = new Wheel[6]();
	mArm = new Arm();
	for (int i = 0; i < 6; i++) {
		mWheels[i].setWheelDesription((Wheel::wheelDescription) i);
	}
	mArm->setActive(false);
}
Wheel::wheelDescription Rover::getWheel(Wheel::wheelDescription whichWheel) {

	for (int i = 0; i < 6; i++) {
		if (mWheels[i].getWheelDesription() == whichWheel)
			return mWheels[i].getWheelDesription();
	}

}

void Rover::setWheelsDiameter(double newDiameter) {
	_wheelsDiameter = newDiameter;
}

double Rover::getWheelsDiameter() {
	return _wheelsDiameter;
}

void Rover::setWheelPower(Wheel::wheelDescription whichWheel, double newPower) {
	for (int i = 0; i < 6; i++) {
		if (mWheels[i].getWheelDesription() == whichWheel)
			mWheels[i].setWheelPower(newPower);
	}
}

double Rover::getWheelPower(Wheel::wheelDescription whichWheel) {
	for (int i = 0; i < 6; i++) {
		if (mWheels[i].getWheelDesription() == whichWheel)
			return mWheels[i].getWheelPower();
	}
}

void Rover::setWheelSpeed(Wheel::wheelDescription whichWheel, double newSpeed) {
	for (int i = 0; i < 6; i++) {
		if (mWheels[i].getWheelDesription() == whichWheel)
			mWheels[i].setSpeed(newSpeed);
	}
}

double Rover::getWheelSpeed(Wheel::wheelDescription whichWheel) {
	for (int i = 0; i < 6; i++) {
		if (mWheels[i].getWheelDesription() == whichWheel)
			return mWheels[i].getSpeed();
	}
}

void Rover::setWheelPosition(Wheel::wheelDescription whichWheel, float x, float y, float z) {
	for (int i = 0; i < 6; i++) {
		if (mWheels[i].getWheelDesription() == whichWheel)
			mWheels[i].setPosition(x, y, z);
	}
}

std::vector<float> Rover::getWheelPosition(Wheel::wheelDescription whichWheel) {
	for (int i = 0; i < 6; i++) {
		if (mWheels[i].getWheelDesription() == whichWheel)
			return mWheels[i].getPosition();
	}
}

void Rover::setArmActive(bool isActive) {
	mArm->setActive(isActive);
}

bool Rover::getArmActive() {
	return mArm->getActive();
}

void Rover::setArmLength(double newLength) {
	mArm->setLength(newLength);
}

double Rover::getArmLength() {
	return mArm->getLength();
}

void Rover::setArmFirstAngle(double newAngle) {
	mArm->setFirstAngle(newAngle);
}

double Rover::getArmFirstAngle() {
	return mArm->getFirstAngle();
}

void Rover::setArmSecondAngle(double newAngle) {
	mArm->setSecondAngle(newAngle);
}

double Rover::getArmSecondAngle() {
	return mArm->getSecondAngle();
}

void Rover::setArmThirdAngle(double newAngle) {
	mArm->setThirdAngle(newAngle);
}

double Rover::getArmThirdAngle() {
	return mArm->getThirdAngle();
}

void Rover::setArmFirstPower(double newPower) {
	mArm->setPowerOne(newPower);
}

double Rover::getArmFirstPower() {
	return mArm->getPowerOne();
}

void Rover::setArmSecondPower(double newPower) {
	mArm->setPowerTwo(newPower);
}

double Rover::getArmSecondPower() {
	return mArm->getPowerTwo();
}

void Rover::setArmThirdPower(double newPower) {
	mArm->setPowerThree(newPower);
}

double Rover::getArmThirdPower() {
	return mArm->getPowerThree();
}
Rover::~Rover() {
	std::cout << "Rover has been deleted." << std::endl;
}
