#include "../../include/LemRover/model/arm.hpp"
#include <iostream>
using namespace LemRover;


Arm::Arm()
{
	std::cout << "Arm is being created." << std::endl;
}

void Arm::setActive(bool isActive)
{
	_isActive = isActive;
}

bool Arm::getActive()
{
	return _isActive;
}

void Arm::setFirstAngle(double firstAngle)
{
	_firstAngle = firstAngle;
}

double Arm::getFirstAngle()
{
	return _firstAngle;
}

void Arm::setSecondAngle(double secondAngle)
{
	_secondAngle = secondAngle;
}

double Arm::getSecondAngle()
{
	return _secondAngle;
}

void Arm::setThirdAngle(double thirdAngle)
{
	_thirdAngle = thirdAngle;
}

double Arm::getThirdAngle()
{
	return _thirdAngle;
}

void Arm::setLength(double length)
{
	_length = length;
}

double Arm::getLength()
{
	return _length;
}

void Arm::setPowerOne(double power)
{
	_powerOne = power;
}

double Arm::getPowerOne()
{
	return _powerOne;
}

void Arm::setPowerTwo(double power)
{
	_powerTwo = power;
}

double Arm::getPowerTwo()
{
	return _powerTwo;
}

void Arm::setPowerThree(double power)
{
	_powerThree = power;
}

double Arm::getPowerThree()
{
	return _powerThree;
}

Arm::~Arm()
{
	std::cout << "Arm is being deleted." << std::endl;
}


