#include <ros/ros.h>
#include <ros/network.h>
#include <string>
#include <std_msgs/String.h>
#include <geometry_msgs/Twist.h>
#include <sstream>
#include "../include/LemRover/qnode.hpp"

namespace LemRover {

QNode::QNode(int argc, char** argv ) :
	init_argc(argc),
	init_argv(argv)
    {
    angZ = angX = angY = 0.0f;
}

QNode::~QNode() {
    if(ros::isStarted()) {
      ros::shutdown(); // explicitly needed since we use ros::start();
      ros::waitForShutdown();
    }
	wait();
}

void chatterCallback(const geometry_msgs::Twist::ConstPtr& msg)
{
    qWarning("Angular: [%.f,%.f,%.f] Linear: [%.f,%.f,%.f]", msg->angular.x,msg->angular.y,msg->angular.z, msg->linear.x,msg->linear.y,msg->linear.z);
}

bool QNode::init() {
	ros::init(init_argc,init_argv,"LemRover");
	if ( ! ros::master::check() ) {
		return false;
	}
    ros::start();
    ros::NodeHandle n;

    chatter_publisher = n.advertise<geometry_msgs::Twist>("cmd_vel", 10);
    //chatter_subscriber = n.subscribe("cmd_vel", 10, chatterCallback);
	start();
	return true;
}

bool QNode::init(const std::string &master_url, const std::string &host_url) {
	std::map<std::string,std::string> remappings;
	remappings["__master"] = master_url;
	remappings["__hostname"] = host_url;
	ros::init(remappings,"LemRover");
	if ( ! ros::master::check() ) {
		return false;
	}
    ros::start();
    ros::NodeHandle n;

    chatter_publisher = n.advertise<geometry_msgs::Twist>("cmd_vel", 10);
    //chatter_subscriber = n.subscribe("cmd_vel", 10, chatterCallback);
	start();
	return true;
}

void QNode::run() {
    ros::Rate loop_rate(10);
	int count = 0;
    while ( ros::ok() ) {
        geometry_msgs::Twist newTwist;

        newTwist.angular.x=angX;
        newTwist.angular.y=angY;
        newTwist.angular.z=angZ;

        chatter_publisher.publish(newTwist);

		ros::spinOnce();
        loop_rate.sleep();
		++count;
	}
	std::cout << "Ros shutdown, proceeding to close the gui." << std::endl;
    Q_EMIT rosShutdown();
}

void QNode::publishVel(float velX, float velY, float velZ){
    angZ= velZ;
    angY= velY;
    angX= velX;
    //std_msgs::String msg;
    //std::stringstream ss;
    //ss << "hello world " << count;
    //msg.data = ss.str();
    //chatter_publisher.publish(msg);
//		log(Info,std::string("I sent: ")+msg.data);
}


void QNode::log( const LogLevel &level, const std::string &msg) {
    ROS_DEBUG_STREAM(msg);
}

}  // namespace LemRover
