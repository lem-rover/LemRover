#include "../include/LemRover/window.hpp"
#include "../include/LemRover/gui/openglscene.hpp"

#include <QGLWidget>

using namespace LemRover;
/**
 * @brief Window
 *
 * Konstruktor tej klasy tworzy Okno główne programu.
 * W oparciu o budowę tego okna możemy dodawać i odejmować elementy
 * które będą tworzyły gui. Elementy te powinny dziedziczyć klasę
 * Abstrakcyjną nardzędzia.
 */
Window::Window(int argc, char** argv, QWidget *parent)
    : QMainWindow(parent)
    , qnode(argc,argv){
    lemWidget = new RoverWidget( this );
    lemWidget->setViewport(new QGLWidget(QGLFormat(QGL::SampleBuffers)));
    lemWidget->setViewportUpdateMode(QGraphicsView::FullViewportUpdate);
    lemWidget->setScene(new OpenGLScene);
    setCentralWidget( lemWidget );

    mGamepadDisabled = new QIcon( ":/resources/gamepad-red.png" );
    mGamepadEnabled = new QIcon( ":/resources/gamepad-green.png" );
    isGamepadEnabled = false;

    createActions();
    createMenus();
    createToolBars();
    createStatusBar();
    createDockWindows();
    //createROSControl();
    connectROS();

    setWindowTitle( tr( "Lem Mars Rover" ) );

    //connect( this, &MainWindow::signal_send_from_main, &Mserver, &MyServer::slot_set_data );
}

void Window::updateMenus()
{
        bool hasMdiChild = ( activeMdiChild() != 0 );
        actionSwitchGamepad->setEnabled( hasMdiChild );
    //    saveAsAct->setEnabled(hasMdiChild);
    //#ifndef QT_NO_CLIPBOARD
    //    pasteAct->setEnabled(hasMdiChild);
    //#endif
    //    closeAct->setEnabled(hasMdiChild);
    //    closeAllAct->setEnabled(hasMdiChild);
    //    tileAct->setEnabled(hasMdiChild);
    //    cascadeAct->setEnabled(hasMdiChild);
    //    nextAct->setEnabled(hasMdiChild);
    //    previousAct->setEnabled(hasMdiChild);
    //    separatorAct->setVisible(hasMdiChild);

    //#ifndef QT_NO_CLIPBOARD
    //    bool hasSelection = (activeMdiChild() /*&&
    //                         activeMdiChild()->textCursor().hasSelection()*/);
    //    cutAct->setEnabled(hasSelection);
    //    copyAct->setEnabled(hasSelection);
    //#endif
}

InternalWindow *Window::activeMdiChild()
{
    if ( QMdiSubWindow *activeSubWindow = panelCentral->activeSubWindow() ) {
        return qobject_cast<InternalWindow *>( activeSubWindow->widget() );
    }

    return 0;
}

void Window::connectROS(){
    ReadSettings();
    QObject::connect(&qnode, SIGNAL(rosShutdown()), this, SLOT(close()));

    /*********************
    ** Logging
    **********************/
    //view_logging->setModel(qnode.loggingModel());
    //QObject::connect(&qnode, SIGNAL(loggingUpdated()), this, SLOT(updateLoggingView()));

    /*********************
    ** Auto Start
    **********************/
    if ( checkbox_remember_settings->isChecked() ) {
        on_button_connect_clicked(true);
    }
}

void Window::ReadSettings() {
    QSettings settings("Qt-Ros Package", "LemRover");
    restoreGeometry(settings.value("geometry").toByteArray());
    restoreState(settings.value("windowState").toByteArray());
    QString master_url = settings.value("master_url",QString("http://192.168.1.2:11311/")).toString();
    QString host_url = settings.value("host_url", QString("192.168.1.3")).toString();
    //QString topic_name = settings.value("topic_name", QString("/chatter")).toString();
    line_edit_master->setText(master_url);
    line_edit_host->setText(host_url);
    //ui.line_edit_topic->setText(topic_name);
    bool remember = settings.value("remember_settings", false).toBool();
    checkbox_remember_settings->setChecked(remember);
    bool checked = settings.value("use_environment_variables", false).toBool();
    checkbox_use_environment->setChecked(checked);
    if ( checked ) {
        line_edit_master->setEnabled(false);
        line_edit_host->setEnabled(false);
        //ui.line_edit_topic->setEnabled(false);
    }
}

void Window::showNoMasterMessage() {
    QMessageBox msgBox;
    msgBox.setText("Couldn't find the ros master.");
    msgBox.exec();
    //close();
}

void Window::on_button_connect_clicked(bool check ) {
    if ( checkbox_use_environment->isChecked() ) {
        if ( !qnode.init() ) {
            showNoMasterMessage();
            button_connect->setEnabled(true);
        } else {
            button_connect->setEnabled(false);
        }
    } else {
        if ( ! qnode.init(line_edit_master->text().toStdString(),
                   line_edit_host->text().toStdString()) ) {
            showNoMasterMessage();
            button_connect->setEnabled(true);
            line_edit_master->setReadOnly(false);
            line_edit_host->setReadOnly(false);
            line_edit_topic->setReadOnly(false);
        } else {
            button_connect->setEnabled(false);
            line_edit_master->setReadOnly(true);
            line_edit_host->setReadOnly(true);
            line_edit_topic->setReadOnly(true);
        }
    }
}

void Window::on_checkbox_use_environment_stateChanged(int state) {
    bool enabled;
    if ( state == 0 ) {
        enabled = true;
    } else {
        enabled = false;
    }
    line_edit_master->setEnabled(enabled);
    line_edit_host->setEnabled(enabled);
    //line_edit_topic->setEnabled(enabled);
}

void Window::WriteSettings() {
    QSettings settings("Qt-Ros Package", "LemRover");
    settings.setValue("master_url",line_edit_master->text());
    settings.setValue("host_url",line_edit_host->text());
    //settings.setValue("topic_name",line_edit_topic->text());
    settings.setValue("use_environment_variables",QVariant(checkbox_use_environment->isChecked()));
    settings.setValue("geometry", saveGeometry());
    settings.setValue("windowState", saveState());
    settings.setValue("remember_settings",QVariant(checkbox_remember_settings->isChecked()));
}

void Window::closeEvent(QCloseEvent *event)
{
    WriteSettings();
    QMainWindow::closeEvent(event);
}

void Window::createMenus()
{
    fileMenu = menuBar()->addMenu( tr( "&File" ) );
    fileMenu->setObjectName(QStringLiteral("MenuFile"));
    fileMenu->addAction( newLetterAct );
    fileMenu->addAction( actionSwitchGamepad );
    fileMenu->addSeparator();
    fileMenu->addAction( actionQuit );
    viewMenu = menuBar()->addMenu( tr( "&View" ) );
    viewMenu->setObjectName(QStringLiteral("ViewMenu"));
    menuBar()->addSeparator();

    helpMenu = menuBar()->addMenu( tr( "&Help" ) );
    helpMenu->setObjectName(QStringLiteral("ViewHelp"));
    helpMenu->addAction( aboutAct );
}

void Window::createToolBars()
{
    fileToolBar = addToolBar( tr( "File" ) );
    fileToolBar->setObjectName(QStringLiteral("File"));
    fileToolBar->addAction( newLetterAct );
    fileToolBar->addAction( actionSwitchGamepad );
}

void Window::createStatusBar()
{
    statusBar()->showMessage( tr( "Ready" ) );
}

void Window::createDockWindows()
{
    QDockWidget *dock = new QDockWidget( tr( "Element Info" ), this );
    dock->setObjectName(QStringLiteral("Element_Info"));
    dock->setAllowedAreas( Qt::RightDockWidgetArea );
    informationList = new QListWidget( dock );
    informationList->addItems( QStringList() << "Select element to view info." );

    dock->setWidget( informationList );
    addDockWidget( Qt::RightDockWidgetArea, dock );
    viewMenu->addAction( dock->toggleViewAction() );

    dock = new QDockWidget( tr( "Pad Information" ), this );
    dock->setObjectName(QStringLiteral("Pad_Information"));
    barX = new QProgressBar( );
    barX->setValue( 50 );

    dock->setWidget( barX );
    addDockWidget( Qt::RightDockWidgetArea, dock );
    viewMenu->addAction( dock->toggleViewAction() );

    dock = new QDockWidget( tr( "Connect" ), this );
    dock->setObjectName(QStringLiteral("dock_status"));
    QSizePolicy sizePolicy1(QSizePolicy::Minimum, QSizePolicy::Preferred);
    sizePolicy1.setHorizontalStretch(0);
    sizePolicy1.setVerticalStretch(0);
    sizePolicy1.setHeightForWidth(dock->sizePolicy().hasHeightForWidth());
    dock->setSizePolicy(sizePolicy1);
    dock->setMinimumSize(QSize(325, 441));
    dock->setAllowedAreas(Qt::RightDockWidgetArea);
    createROSControl();
    dock->setWidget( dockWidgetContents_2 );
    addDockWidget(static_cast<Qt::DockWidgetArea>(2), dock);
    connect( informationList, SIGNAL( currentTextChanged( QString ) ), this, SLOT( insertCustomer( QString ) ) );
    QMetaObject::connectSlotsByName(this);
}

void Window::createROSControl(){
    dockWidgetContents_2 = new QWidget();
    dockWidgetContents_2->setObjectName(QStringLiteral("dockWidgetContents_2"));
    verticalLayout = new QVBoxLayout(dockWidgetContents_2);
    verticalLayout->setObjectName(QStringLiteral("verticalLayout"));
    QSizePolicy sizePolicy1(QSizePolicy::Minimum, QSizePolicy::Preferred);
    frame = new QFrame(dockWidgetContents_2);
    frame->setObjectName(QStringLiteral("frame"));
    sizePolicy1.setHeightForWidth(frame->sizePolicy().hasHeightForWidth());
    frame->setSizePolicy(sizePolicy1);
    frame->setFrameShape(QFrame::StyledPanel);
    frame->setFrameShadow(QFrame::Raised);
    verticalLayout_3 = new QVBoxLayout(frame);
    verticalLayout_3->setObjectName(QStringLiteral("verticalLayout_3"));
    groupBox = new QGroupBox(frame);
    groupBox->setObjectName(QStringLiteral("groupBox"));
    gridLayout = new QGridLayout(groupBox);
    gridLayout->setObjectName(QStringLiteral("gridLayout"));
    label = new QLabel(groupBox);
    label->setObjectName(QStringLiteral("label"));
    label->setFrameShape(QFrame::StyledPanel);
    label->setFrameShadow(QFrame::Raised);

    gridLayout->addWidget(label, 0, 0, 1, 1);

    line_edit_master = new QLineEdit(groupBox);
    line_edit_master->setObjectName(QStringLiteral("line_edit_master"));

    gridLayout->addWidget(line_edit_master, 1, 0, 1, 2);

    label_2 = new QLabel(groupBox);
    label_2->setObjectName(QStringLiteral("label_2"));
    label_2->setFrameShape(QFrame::StyledPanel);
    label_2->setFrameShadow(QFrame::Raised);

    gridLayout->addWidget(label_2, 2, 0, 1, 1);

    line_edit_host = new QLineEdit(groupBox);
    line_edit_host->setObjectName(QStringLiteral("line_edit_host"));

    gridLayout->addWidget(line_edit_host, 3, 0, 1, 2);

    label_3 = new QLabel(groupBox);
    label_3->setObjectName(QStringLiteral("label_3"));
    label_3->setFrameShape(QFrame::StyledPanel);
    label_3->setFrameShadow(QFrame::Raised);

    gridLayout->addWidget(label_3, 4, 0, 1, 1);

    line_edit_topic = new QLineEdit(groupBox);
    line_edit_topic->setObjectName(QStringLiteral("line_edit_topic"));
    line_edit_topic->setEnabled(false);

    gridLayout->addWidget(line_edit_topic, 5, 0, 1, 2);

    checkbox_use_environment = new QCheckBox(groupBox);
    checkbox_use_environment->setObjectName(QStringLiteral("checkbox_use_environment"));
    checkbox_use_environment->setLayoutDirection(Qt::RightToLeft);

    gridLayout->addWidget(checkbox_use_environment, 6, 0, 1, 2);

    checkbox_remember_settings = new QCheckBox(groupBox);
    checkbox_remember_settings->setObjectName(QStringLiteral("checkbox_remember_settings"));
    checkbox_remember_settings->setLayoutDirection(Qt::RightToLeft);

    gridLayout->addWidget(checkbox_remember_settings, 7, 0, 1, 2);

    button_connect = new QPushButton(groupBox);
    button_connect->setObjectName(QStringLiteral("button_connect"));
    button_connect->setEnabled(true);
    QSizePolicy sizePolicy2(QSizePolicy::MinimumExpanding, QSizePolicy::Fixed);
    sizePolicy2.setHorizontalStretch(0);
    sizePolicy2.setVerticalStretch(0);
    sizePolicy2.setHeightForWidth(button_connect->sizePolicy().hasHeightForWidth());
    button_connect->setSizePolicy(sizePolicy2);

    gridLayout->addWidget(button_connect, 8, 1, 1, 1);


    verticalLayout_3->addWidget(groupBox);

    verticalLayout->addWidget(frame);

    groupBox->setTitle(QApplication::translate("MainWindowDesign", "Ros Master", 0));
    label->setText(QApplication::translate("MainWindowDesign", "Ros Master Url", 0));
    line_edit_master->setText(QApplication::translate("MainWindowDesign", "http://192.168.1.2:11311/", 0));
    label_2->setText(QApplication::translate("MainWindowDesign", "Ros IP", 0));
    line_edit_host->setText(QApplication::translate("MainWindowDesign", "192.168.1.67", 0));
    label_3->setText(QApplication::translate("MainWindowDesign", "Ros Hostname", 0));
    line_edit_topic->setText(QApplication::translate("MainWindowDesign", "unused", 0));
    checkbox_use_environment->setText(QApplication::translate("MainWindowDesign", "Use environment variables", 0));
    checkbox_remember_settings->setText(QApplication::translate("MainWindowDesign", "Remember settings on startup", 0));

#ifndef QT_NO_TOOLTIP
        button_connect->setToolTip(QApplication::translate("MainWindowDesign", "Set the target to the current joint trajectory state.", 0));
#endif // QT_NO_TOOLTIP
#ifndef QT_NO_STATUSTIP
        button_connect->setStatusTip(QApplication::translate("MainWindowDesign", "Clear all waypoints and set the target to the current joint trajectory state.", 0));
#endif // QT_NO_STATUSTIP
        button_connect->setText(QApplication::translate("MainWindowDesign", "Connect", 0));

}

/**
 * @brief createActions
 *
 * Metoda stworzona by w jednym miejscu opakować przypinanie sygnałów do
 * kontrolek menu naszego okna.
 */
void Window::createActions()
{
    // Dodanie nowej opcji do menu powinno być zbudowane jak na przykładzie poniżej:
    // Do zadeklarowanej wcześniej zmiennej, która powinna znajdować się w pliku window.h
    // przypisujemy nowo stworzoną Akcję(QAction) która wykona się po kliknięciu opcji Quit w menu.
    actionQuit = new QAction( tr( "&Quit" ), this );
    // Do wybranej akcji dodajemy również skrót metodą setShortcuts(QKeySequence::<klawisz>).
    actionQuit->setShortcuts( QKeySequence::Quit );
    // Dla wygody i pełności interfejsu dodajemy "Tooltip" - jest to napis który wyświetli się
    // po najechaniu myszką na tą opcję w menu. Powinien on zawierać krótkie wyjaśnienie naszej akcji.
    actionQuit->setStatusTip( tr( "Quit the application" ) );
    // Podłączenie akcji pod sygnał. Sygnał jest czymś, co jest generowane automatycznie pod wpływem
    // interakcji użytkownika z interfejsem - w skrócie jest to system zdarzeń, niżej do wywołania
    // akcji podłączona jest akcja "close()", która powoduje zamknięcie okna.
    //    connect(actionQuit, SIGNAL(triggered()), this, SLOT(close()));


    newLetterAct = new QAction( QIcon( ":/resources/terminal.png" ), tr( "&Open terminal" ), this );
    newLetterAct->setShortcuts( QKeySequence::New );
    newLetterAct->setStatusTip( tr( "Open terminal" ) );
    //    connect(newLetterAct, SIGNAL(triggered()), this, SLOT(openTerminal()));

    actionSwitchGamepad = new QAction( *mGamepadDisabled, tr( "&Enable Gamepad" ), this );
    actionSwitchGamepad->setShortcuts( QKeySequence::Save );
    actionSwitchGamepad->setStatusTip( tr( "Enable/Disable Gamepad" ) );
    connect( actionSwitchGamepad, SIGNAL( triggered() ), this, SLOT( switchGamepad() ) );

    aboutAct = new QAction( tr( "&About" ), this );
    aboutAct->setStatusTip( tr( "Show the application's About box" ) );
    //    connect(aboutAct, SIGNAL(triggered()), this, SLOT(about()));
}

void Window::switchGamepad()
{
    isGamepadEnabled = !isGamepadEnabled;

    if ( isGamepadEnabled ) {
        actionSwitchGamepad->setIcon( *mGamepadEnabled );
            mController = new Controller();
            connect( mController, &Controller::signal_controller_connection_closed,
                     this, &Window::slot_controller_connection_closed );
            connect( mController, &Controller::signal_up_controller_value_changed,
                     this, &Window::slot_controller_value_changed );
            connect( mController, &Controller::signal_up_got_Gamepad_info,
                     this, &Window::slot_got_Gamepad_info );
            connect( this, &Window::signal_keyboard_val_change,
                     mController, &Controller::slot_keyboard_value_changed );
            mController->set_type( "Gamepad" );
    } else {
        actionSwitchGamepad->setIcon( *mGamepadDisabled );
            if ( mController != NULL ) {
                mController->set_type( "Keyboard" );
            }
    }
}

void Window::slot_controller_value_changed( QString lo )
{
    QRegExp rx( "(\\;)" ); //RegEx for ' ' or ',' or '.' or ':' or '\t'
    QStringList query = lo.split( rx );
    QString what = query.at( 0 );
    QString val = query.at( 1 );
    QString x = "axis0";

    if ( !QString::compare( what,x ) ) {
        float realValue = val.toFloat() + 32767.0;
        barX->setValue( ( realValue / 65534 ) * 100 );
        qnode.publishVel(0.0f, 0.0f, ( val.toFloat() / 65534 ));
    }
    //emit signal_send_from_main( lo );
}

void Window::slot_controller_connection_closed()
{

}

void Window::slot_got_Gamepad_info( QStringList info )
{
    QString set;

    if ( !isGamepadEnabled ) {
        set = QString( "Controller: Keyboard" );
    } else if ( info[1] == "" ) {
        set = QString( "Name: %1" ).arg( info[0] );
    } else {
        set = QString( "Controller: %1" ).arg( info[0] );
    }

        //lemWidget->printControl( &set );
}

void Window::slot_set_connected()
{
    //    connection = true;
    //    ui->interface_status_label->setText("<font color='Green'>Connected</font>");

}

void Window::slot_set_disconnected()
{
    //    connection = false;
    //    ui->interface_status_label->setText("<font color='Red'>Disconnected</font>");
    }
