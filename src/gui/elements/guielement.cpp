#include "../../../include/LemRover/gui/elements/guielement.hpp"
#include "../../../include/LemRover/gui/widgets/roverwidget.hpp"

#include <QtWidgets>
#include <QtCore>

using namespace LemRover;

GuiElement::GuiElement( RoverWidget *pWidget, int pWidth , int pHeight )
    : mWidget( pWidget )
{
    setFlag( ItemIsMovable );
    setFlag( ItemSendsGeometryChanges );
    setCacheMode( DeviceCoordinateCache );
    setZValue( -1 );

    mWidth = pWidth;
    mHeight = pHeight;
    mPos = new QPointF( 10, 10 );
}

void GuiElement::setPosition( int pX, int pY )
{
    mPos = new QPointF( pX, pY );
}

void GuiElement::setInfo( QString *set )
{
    mCurrentInformation = *set;
    update();
}

void GuiElement::setSize( int pWidth, int pHeight )
{
    mWidth = pWidth;
    mHeight = pHeight;
}

QPixmap *GuiElement::getPixelMap()
{
    return mPixelmap;
}

QRectF GuiElement::boundingRect() const
{
    qreal adjust = 2;

    return QRectF( mPos->x(), mPos->y(), mWidth, mHeight );
}

QPainterPath GuiElement::shape() const
{
    QPainterPath path;
    path.addRect( mPos->x(), mPos->y(), mWidth, mHeight );

    return path;
}

void GuiElement::paint( QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget * )
{
    int offset = 0;

    if ( mCurrentInformation == NULL) {
        offset = (sSpacing + sMargin +4);
    }

    // Shadow
    QRectF sceneRect( mPos->x(), mPos->y(), mWidth, mHeight );
    QRectF textRect( mPos->x() + sSpacing + sMargin + offset, mPos->y()  + sSpacing + sMargin + offset, mWidth - 2 * sMargin, mHeight - 2 * sMargin );

    //Background
    QLinearGradient gradient( sceneRect.topLeft(), sceneRect.bottomRight() );
    gradient.setColorAt( 0, Qt::white );
    gradient.setColorAt( 1, Qt::lightGray );
    painter->fillRect( sceneRect, gradient );
    painter->setBrush( Qt::NoBrush );
    painter->drawRect( sceneRect );

    QFont font = painter->font();
    font.setBold( false );
    font.setPointSize( 9 );
    painter->setFont( font );
    painter->setPen( Qt::black );

    if ( mCurrentInformation == NULL ) {
        painter->drawText( textRect,  "Unknown" );
    } else {
        painter->drawText( textRect, mCurrentInformation );
    }
}

void GuiElement::mouseMoveEvent( QGraphicsSceneMouseEvent *event )
{
    //    if (mType == NodeBody) {
    //        if (isPressed) {
    //            QPointF nowPoint = event->pos();
    //            QPointF lastPoint = event->lastPos();
    //            qreal difX = nowPoint.x() - lastPoint.x();
    //            qreal difY = nowPoint.y() - lastPoint.y();
    //            foreach (QGraphicsItem * item, scene()->items()) {
    //                GraphicsElement *node = qgraphicsitem_cast<GraphicsElement *>(item);
    //                if (!node)
    //                    continue;

    //                if (node->mType != NodeBody) {
    //                    node->moveBy(difX, difY);
    //                }
    //            }
    //        }
    //    }
    update();
    QGraphicsItem::mouseMoveEvent( event );
}

void GuiElement::mousePressEvent( QGraphicsSceneMouseEvent *event )
{
    //    isPressed = true;
    //    update();
    update();
    QGraphicsItem::mousePressEvent( event );
}

void GuiElement::mouseReleaseEvent( QGraphicsSceneMouseEvent *event )
{
    //    isPressed = false;
    //    update();
    update();
    QGraphicsItem::mouseReleaseEvent( event );
}

