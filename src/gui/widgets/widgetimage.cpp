#include "../../../include/LemRover/gui/widgets/widgetimage.hpp"

using namespace LemRover;
WidgetImage::WidgetImage(QWidget *parent) : QWidget(parent)
{
    mLabel = new QLabel(this);

}

bool WidgetImage::loadImage(QString &pPath) {

    mPixelMap = new QPixmap(pPath);
    if (!mPixelMap->isNull()) {
        mLabel->setPixmap(*mPixelMap);
        return true;
    }

    return true;
}

