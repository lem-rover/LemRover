#include "../../include/LemRover/logic/controller.hpp"
#include "../../include/LemRover/logic/gamepads/dualshok3.hpp"
#include "../../include/LemRover/logic/keyboard.hpp"

using namespace LemRover;
Controller::Controller(QObject *parent) : QObject(parent)
{
}

void Controller::set_type(QString typ) {
    qDebug()<<typ;
//    delete actual_controller;
    if (typ == "Keyboard")
    {
        Controllerinfo << "Keyboard"<<""<<""<<"";
        actual_controller = new Keyboard();
        Q_EMIT signal_up_got_Gamepad_info(Controllerinfo);

    }
    else if (typ == "Gamepad")
    {
        pad_reader = new Padreader();
        if(pad_reader->active==true)
            pad_reader->start();
        connect(pad_reader,&Padreader::signal_controller_connection_closed, this, &Controller::slot_controller_connection_closed);
        connect(pad_reader, &Padreader::signal_controller_value_changed, this, &Controller::slot_controller_value_changed);
        connect(pad_reader, &Padreader::signal_got_gamepad_info, this, &Controller::slot_got_Gamepad_info);
    }
}


void Controller::slot_got_Gamepad_info(QStringList info){
    Controllerinfo = info;
    if(Controllerinfo[0]=="Sony PLAYSTATION(R)3 Controller"){
        actual_controller = new DualShok3();
    }
    Q_EMIT signal_up_got_Gamepad_info(Controllerinfo);
}

void Controller::slot_controller_value_changed(QString type, int number, int value){
    if(!(Controllerinfo.isEmpty())){
        if(Controllerinfo[0]=="Sony PLAYSTATION(R)3 Controller"){
            QString lo = actual_controller->xmlcreator(type, number, value);
            qDebug ()<<lo;
            if (lo == "not important"){
            } else {
                Q_EMIT signal_up_controller_value_changed(lo);
            }
        }else{
            QString lo = QString("%1%2;%4;\n").arg(type,QString::number(number),QString::number(value));
            Q_EMIT signal_up_controller_value_changed(lo);
        }
    }
}

void Controller::slot_keyboard_value_changed(QString type){
    if(!(Controllerinfo.isEmpty())){
        QString lo = actual_controller->xmlcreator(type, 0, 120);
        qDebug ()<<lo;
        if (lo == "not important"){
        } else {
            Q_EMIT signal_up_controller_value_changed(lo);
        }

    }
}

void Controller::slot_controller_connection_closed(){
    Q_EMIT signal_controller_connection_closed();
}

void Controller::slot_destroy_pad(){
    Q_EMIT sinal_destroy_pad();
}
