#include "../../include/LemRover/logic/keyboard.hpp"
#include <QtCore/QtDebug>

using namespace LemRover;
Keyboard::Keyboard()
{
     turn=1;
     rotation =0;
     linearSpeed=0;
}

void Keyboard::setTurn(int sturn)
{
}

void Keyboard::setLinearSpeed(int slinearSpeed)
{
    linearSpeed =slinearSpeed;
}

void Keyboard::setRotation(int srotation)
{
    rotation=srotation;
}


QString Keyboard::xmlcreator(QString type, int number, int value){
    qDebug() << type;

    if (type=="W"){
        setRotation(0);
        setLinearSpeed(lin_speed);
    }
    else if (type=="S"){
        setRotation(0);
        setLinearSpeed(-lin_speed);
    }
    else if (type=="A"){
        setRotation(-rot);
        setLinearSpeed(0);
    }
    else if (type=="D"){
        setRotation(rot);
        setLinearSpeed(0);
    }

    else if (type==" "){
        setRotation(0);
        setLinearSpeed(0);
    }
    else {
        qDebug()<<type;
        return "not important";
    }

    QString str = QString(";%1,%2;\n").arg( QString::number(turn*linearSpeed), QString::number(rotation));
    return str;
}
